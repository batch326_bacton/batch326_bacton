<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//access the authenticated user via the Auth facade
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
class PostController extends Controller
{

    //Controller is for the user logout
    public function logout(){
        // it will logout the currently logged in user.
        Auth::logout();
        return redirect('/login');
    }

    //action to return a view containing a form for a blog post creation
    public function createPost(){
        return view('posts.create');
    }

    public function savePost(Request $request){
        //to check whether there is an authenticated user or none
        if(Auth::user()){
            //instantiate a new Post Oject from the Post method and then save it in $post variable:
            $post = new Post;

            //define the properties of the $post object using the received form data
            $post->title=$request->input('title');
            $post->body=$request->input('content');

            //this will get the id of the authenticated user and set it as the foreign jet user_id of the new post.
            $post->user_id=(Auth::user()->id);

            //save the post object in our Post Table;
            $post->save();

            return redirect('/posts/create');
        }else{
            return redirect('/login');
        }
    }

    //controller will return all the blog posts
    public function showPosts(){
        //the all() method will save all the records in our Post Table in our $post variable
        $posts = Post::all();

        return view('posts.showPosts')->with('posts', $posts);
    }
}
